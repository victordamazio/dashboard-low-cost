# Dashboard Low Cost

<div align = 'justify'>

# Como fazer um sistema Node-RED + MySQL + Grafana

## Descrição

Nesse relatório, é descrito um guia de como instalar o Node-RED, o MySQL e o Grafana, e depois fazer os 3 funcionarem em conjunto.

O Node-RED é uma plataforma de programação visual para conectar hardwares, útil para internet das coisas, com foco em flows, o MySQL é um sistema de banco de dados baseado em SQL, e o Grafana é uma aplicação para gráficos e dashboards.

A ideia é fazer um ou mais flows para coletar dados dos nossos dispositivos, passar os dados para um banco de dados MySQL, e depois visualizar os dados num dashboard Grafana, este é apenas um tutorial básico de como fazer os 3 funcionarem em conjunto, não entra em detalhes sobre funções mais complexas das 3 ferramentas.

### Passos:

__1. Ter Linux/Ubuntu instalado, se estiver usando Windows, é recomendado instalar uma máquina virtual usando o VirtualBox.__

Guia para instalar Ubuntu: https://www.vmia.com.br/blog/index.php/2020/05/29/como-instalar-ubuntu-server-20-04-no-virtualbox/

__2. Instalar e rodar o Node-RED__

Guia para instalar e rodar o Node-Red: https://nodered.org/docs/getting-started/local

__3. Instalar e rodar o MySQL__

Guia para instalar e rodar o MySQL: https://www.digitalocean.com/community/tutorials/how-to-install-mysql-on-ubuntu-20-04-pt

__4. Como recomendação, instale o MySQL Workbench, para facilitar o trabalho com uma interface gráfica para o MySQL:__

Guia para instalar o MySQL Workbench: https://dev.mysql.com/doc/workbench/en/wb-installing-linux.html

__5. Instalar e rodar o Grafana__

Guia para instalar e rodar o Grafana: https://grafana.com/docs/grafana/latest/installation/debian/

Instale o Grafana gratuito, não o Grafana Enterprise.

__6. Após certificar que as três ferramentas estão instaladas e rodando corretamente, abra o Node-RED no navegador, colocando http://localhost:1880 na barra de endereço, e também o Grafana, colocando http://localhost:3000, recomendo também abrir o MySQL Workbench.__

__7. Crie uma tabela SQL, com uma Primary Key e uma Timestamp automática, como essa aqui:__

![image](https://user-images.githubusercontent.com/25162231/101772504-32803580-3aca-11eb-8bde-7c60f89f270f.png)

Como criar uma tabela com Timestamp automática, que sempre vai começar com a data e hora de quando a tabela foi atualizada:

https://dev.mysql.com/doc/refman/5.6/en/timestamp-initialization.html

__8. No Node-RED, vá na barra de configurações, e clique em “Manage palette”__

![image](https://user-images.githubusercontent.com/25162231/101772516-3613bc80-3aca-11eb-8c63-e9d15800d424.png)

Pesquise por SQL, e depois instale o MySQL Node, na figura, ele já está instalado

![image](https://user-images.githubusercontent.com/25162231/101772543-3f048e00-3aca-11eb-8fee-b4b7657b0fee.png)

__9. Crie um flow assim:__

![image](https://user-images.githubusercontent.com/25162231/101772559-42981500-3aca-11eb-9685-18ef5d19d2c6.png)

Primeiro um node Inject, para começar o flow, depois um node Function, depois um node MySQL, depois um node Debug.

O node function precisa ter uma Query SQL, um exemplo da qual está sendo usada

```
msg.topic="INSERT INTO ‘schema_test_nodered’.’thermometer’ (`temperature1`, `temperature2`) VALUES (‘25’, ‘26’)"
return msg;
```

![image](https://user-images.githubusercontent.com/25162231/101772574-4a57b980-3aca-11eb-9e5f-2be8952fdd32.png)

Preencha o node MySQL assim de acordo com o seu banco, colocando o usuário, senha, e nome do banco:

![image](https://user-images.githubusercontent.com/25162231/101772585-4d52aa00-3aca-11eb-874e-9d84079ffd3e.png)

__10. Clique no node Inject para inserir os dados na sua tabela SQL, e verifique se a sua tabela está com os dados corretos.__

__11. Após confirmar que o flow Node-RED está conseguindo inserir dados na sua tabela SQL, configure o Grafana, vá em Configuration → Data Sources → Add Data Source → MySQL__

E depois, preencha assim

![image](https://user-images.githubusercontent.com/25162231/101772598-504d9a80-3aca-11eb-8cb0-4da4fb73ea36.png)

__12. Depois, crie um Dashboard (Manage → New Dashboard) e um Panel (Add new panel)__

![image](https://user-images.githubusercontent.com/25162231/105722394-5ece0500-5f04-11eb-9ae7-585f60e97c9c.png)
![image](https://user-images.githubusercontent.com/25162231/105722595-96d54800-5f04-11eb-8564-0ce501af96f9.png)
![image](https://user-images.githubusercontent.com/25162231/105722972-021f1a00-5f05-11eb-817a-5cadae0c325c.png)

__13. No Panel que você vai usar (Edit Panel para configurar o Panel), crie uma Query com o Data Source que você criou (Se precisar, clique em + Query), configure os dados que serão lidos, e deixe eles ordenados pelo Timestamp__

![image](https://user-images.githubusercontent.com/25162231/101772604-53e12180-3aca-11eb-9442-7c448b3e7910.png)

__14. Terminado, você pode mexer em mais algumas configurações para definir como visualizar os dados, e também marcar o tempo, para o intervalo de datas que definirá quais dados serão exibidos no panel.__

## MQTT

Descrito nessa seção, os passos para fazer um flow que use mensagens MQTT:

__1. Crie um flow assim:__

![image](https://user-images.githubusercontent.com/25162231/102217857-ee22da00-3ebb-11eb-823d-80835b74c8f2.png)

__2. Edite o Inject:__

![image](https://user-images.githubusercontent.com/25162231/102217909-05fa5e00-3ebc-11eb-8f2d-1239601658d8.png)

O número 22 é só um exemplo, você pode escolher outro número ou uma string se quiser.

__3. Edite o mqtt out:__

![image](https://user-images.githubusercontent.com/25162231/102217969-18749780-3ebc-11eb-84cc-a16ab7f99125.png)

![image](https://user-images.githubusercontent.com/25162231/102218006-24605980-3ebc-11eb-845e-6b5ec644baf9.png)

__4. Edite o mqtt in:__

![image](https://user-images.githubusercontent.com/25162231/102218070-37732980-3ebc-11eb-9e03-a9c703b98836.png)

![image](https://user-images.githubusercontent.com/25162231/102218100-42c65500-3ebc-11eb-8281-4d0adfaff637.png)

__5. Aperte o botão Inject, se tudo der certo, o número 22 deve aparecer na tela de debug:__

![image](https://user-images.githubusercontent.com/25162231/102220400-84a4ca80-3ebf-11eb-8335-d6eb4ec9eed8.png)

No momento, esse é apenas um flow de teste, passando uma mensagem MQTT do mqtt out para o mqtt in, usando o mesmo flow.

__6. Agora é a hora de mandar uma mensagem MQTT pelo Mosquitto, primeiro, instale o Mosquitto:__

Digite no terminal do Ubuntu:

```
sudo apt-get update
sudo apt-get upgrade

sudo apt-get install mosquitto
sudo apt-get install mosquitto-clients

```

__7. Envie a mensagem MQTT através do terminal:__

`mosquitto_pub -m 'Teste' - t test/nodered`

O objetivo é mandar uma mensagem mqtt direto para o Node mqtt in

Se der certo, a mensagem "Teste" deve aparecer na tela de debug:

![image](https://user-images.githubusercontent.com/25162231/102222131-c9316580-3ec1-11eb-9437-70275c6eeac1.png)

__8. Crie um flow assim, copie o node MySQL usado anteriormente:__

![image](https://user-images.githubusercontent.com/25162231/102350607-eb8bb780-3f83-11eb-99cd-e639e9242d6c.png)

__9. Edite o inject para ter outro payload:__

![image](https://user-images.githubusercontent.com/25162231/102350705-0fe79400-3f84-11eb-95dd-6d88b1e37f85.png)

Os números do inject não precisam ser 32 e 23, mas tem que ser separados por um ponto e vírgula (;).

__10. Edite o node function SQL Query:__

```
nums = msg.payload.split(";")
num1 = parseFloat(nums[0])
num2 = parseFloat(nums[1])
msg.topic="INSERT INTO `schema_test_nodered`.`thermometer` (`temperature1`, `temperature2`) VALUES ('" + num1 + "','" + num2 + "')"
return msg;
```

Esse código irá separar os dois números pelo ponto e vírgula (;) e colocar em duas variáveis separadas, e depois criar uma mensagem SQL com os dois valores sendo inseridos no banco de dados SQL.

__11. Clique no botão inject, e verifique se os valores corretos estão na tabela.__

__12. Se funcionar, agora tente inserir algo direto no node mqtt in, digite no terminal:__

``mosquitto_pub -m '12;21' - t test/nodered``

__13. Verifique se os valores corretos foram inseridos no banco de dados.__

## HTTP

Descrito nessa seção, como enviar uma mensagem para o flow via HTTP.

__1. Crie um flow assim:__

![image](https://user-images.githubusercontent.com/25162231/102484032-6b795680-4044-11eb-86a9-10ec9bad71d7.png)

__2. Configure o node http in:__

![image](https://user-images.githubusercontent.com/25162231/102483810-09205600-4044-11eb-90e8-b5f3ddcb91c6.png)

__3. Digite essa linha na barra de endereço e aperte enter:__

`localhost:1880/myserver?msg=Hello%20World`

__4. Verifique na janela de debug se a mensagem "Hello World" chegou.__

__5. Crie um flow assim, pode copiar os nodes usados anteriormente__

![image](https://user-images.githubusercontent.com/25162231/102615152-2fadc200-4114-11eb-880f-907f40aac92f.png)

__6. Código do node Function é um pouco diferente:__

```
nums = msg.payload.msg.split(";")
num1 = parseFloat(nums[0])
num2 = parseFloat(nums[1])
msg.topic="INSERT INTO `schema_test_nodered`.`thermometer` (`temperature1`, `temperature2`) VALUES ('" + num1 + "','" + num2 + "')"
return msg;
```

__7. Código do node SQL é o mesmo usado para o mqtt anteriormente:__

__8. Coloque a mensagem na barra de endereço e aperte enter:__

`localhost:1880/myserver?msg=11;12`

Os números 11 e 12 podem ser trocados pelo número que você quiser.

__9. Verifique se os números foram inseridos corretamente no banco de dados MySQL__

## CSV

Descrito nessa seção, como ler um arquivo CSV, ler os dados e inseri-los no banco de dados.

__1. Pra começar, consiga um arquivo CSV com duas colunas com números.__

Para conseguir um arquivo CSV mais rápido, vá para o MySQL Workbench e clique em Export/Import para baixar os próprios dados que você já inseriu, se quiser, abra o arquivo CSV com o Libre Office e edite os números.

![image](https://user-images.githubusercontent.com/25162231/104034218-5d37d980-51af-11eb-8833-6c1f17f23300.png)

__2. Crie um flow assim:__

![image](https://user-images.githubusercontent.com/25162231/104034604-dfc09900-51af-11eb-8329-3f9269229af3.png)

__3. Edite o node file in para encontrar o arquivo CSV que você vai usar__

__4. Edite o node csv com as configurações corretas:__

![image](https://user-images.githubusercontent.com/25162231/104034818-24e4cb00-51b0-11eb-974c-1bc7aec5e2ff.png)

Como por exemplo, separar os dados pela separação correta como vírgula, ponto e vírgula ou tab, e escolher a primeira linha como os nomes das colunas.

__5. No código SQL Query, coloque:__

```
num1 = msg.payload.#nomedacoluna1#
num2 = msg.payload.#nomedacoluna2#
msg.topic="INSERT INTO `schema_test_nodered`.`thermometer` (`temperature1`, `temperature2`) VALUES ('" + num1 + "','" + num2 + "')"
return msg;
```

__6. O node MYSQL é melhor que seja copiado dos exemplos acima.__

__7. Clique em Deploy, clique no Node Inject, e verifique se os dados do CSV foram inseridos corretamente no banco de dados MySQL.__

## XML

Descrito nessa seção, como ler um arquivo XML, ler os dados e inseri-los no banco de dados.

__1. Consiga um arquivo XML, igual no exemplo anterior, você pode copiar o baixar banco de dados que você já usou, e fazer algumas edições, deixando ele menor e com dados diferentes, apenas baixe como XML ao invés de CSV.__

__2. Crie um flow assim:__

![image](https://user-images.githubusercontent.com/25162231/104184609-754b6b00-53f2-11eb-9c98-ac3d246fd870.png)

__3. O Node file in precisa ler o arquivo XML no seu destino correto.__

__4. Os Nodes SQL Query e schema_test_nodered podem ser copiados do exemplo acima usado para o CSV.__

__5. Clique em Deploy, clique no Node Inject, e verifique se os dados do XML foram inseridos corretamente no banco de dados MySQL.__

## Autor

Centro Universitário Facens 

Equipe Técnica *Smart Campus* 
Facens:  
Victor Damázio Costa e Souza
</div>
